// ignore_for_file: unnecessary_new

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iqra/provider/user_state.dart';
import 'package:iqra/services/user.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:restart_app/restart_app.dart';

import '../model/user.dart';
import '../widgets/popup_dialog_route.dart';
import '../widgets/signup_verification_widget.dart';

class SignUpTablet extends ConsumerStatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  SignUpTablet({Key? key}) : super(key: key);

  @override
  ConsumerState<SignUpTablet> createState() => _SignUpTabletState();
}

class _SignUpTabletState extends ConsumerState<SignUpTablet> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  dynamic data;

  bool acceptedController = false;
  bool? accepted;
  String? errorMessage;
  String? namaLengkap;
  String email = "";
  String? username;
  String? password;
  bool obscurePassword = true;

  final dayController = TextEditingController();
  final monthController = TextEditingController();
  final yearController = TextEditingController();

  var toa = RichText(
      text: TextSpan(
    style: TextStyle(
        fontSize: 14,
        fontFamily: "Poppins",
        color: Color.fromRGBO(172, 172, 172, 1)),
    children: const <TextSpan>[
      TextSpan(text: 'Setuju dengan '),
      TextSpan(
          text: 'terms and condition',
          style: TextStyle(
              color: Color.fromRGBO(88, 64, 186, 1),
              decoration: TextDecoration.underline)),
      TextSpan(text: ' iqra')
    ],
  ));

  @override
  Widget build(BuildContext context) {
    final User? _user = ref.watch(UserState.userProvider);

    var size = MediaQuery.of(context).size;
    double leftComponentWidth =
        size.width > 1000 ? 430 + 0.05 * size.width : (size.width - 40) / 2;
    double leftComponentMargin =
        size.height < 660 ? 3 : (size.height > 750 ? 15 : 10);

    // Open verification pop up if user info is in storage
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      if (_user != null) {
        Navigator.of(context).push(PopUpDialogRoute(
            barrierDismissible: false,
            builder: (context) => Center(
                  child: SizedBox(
                    height: 300,
                    width: 500,
                    child: VerificationPopUp(),
                  ),
                )));
      }
    });

    return Container(
        // color: Colors.cyan,
        width: size.width,
        padding:
            EdgeInsets.fromLTRB(size.width * 3 / 40, size.height / 20, 40, 0),
        child: Column(children: [
          Column(
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Row(
                  children: [
                    Image.asset(
                      "assets/images/icon.png",
                      width: 44,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 18),
                      child: Text("Daftar",
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Poppins",
                              color: Colors.black,
                              decoration: TextDecoration.none)),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "Sudah punya akun?",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    SizedBox(
                      width: 20,
                      height: 30,
                      child: VerticalDivider(
                        thickness: 1,
                        color: Colors.black,
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/login');
                      },
                      child: Text(
                        "Masuk",
                        style: TextStyle(
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500),
                      ),
                      style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.fromLTRB(12, 7, 12, 7),
                          primary: Theme.of(context).splashColor,
                          shadowColor: Color.fromARGB(0, 0, 0, 0),
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(7.0),
                          )),
                    ),
                  ],
                )
              ]),
              Row(
                children: [
                  Container(
                    // color: Colors.red,
                    margin: EdgeInsets.only(top: size.height / 25),
                    height: 3 * size.height / 5,
                    width: (size.width - 40) / 2,

                    child: Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Column(
                              mainAxisAlignment: size.width > 1150
                                  ? MainAxisAlignment.spaceBetween
                                  : MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // ignore: prefer_const_literals_to_create_immutables
                              children: [
                                Text(
                                  "Nama Lengkap ",
                                  style: Theme.of(context).textTheme.bodyText2,
                                ),
                                SizedBox(
                                  width: leftComponentWidth,
                                  // height: leftComponentHeight,
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value!.isNotEmpty &&
                                          (value.length > 2)) {
                                        return null;
                                      } else {
                                        return "Nama harus lebih dari 2 huruf";
                                      }
                                    },
                                    onChanged: (value) => {
                                      setState(() {
                                        namaLengkap = value;
                                      })
                                    },
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.0)),
                                        isDense: true),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ),
                                // Spacer(flex: 1),
                                Container(
                                  margin:
                                      EdgeInsets.only(top: leftComponentMargin),
                                  child: Text(
                                    "Email ",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                ),
                                SizedBox(
                                  width: leftComponentWidth,
                                  // height: leftComponentHeight,
                                  child: TextFormField(
                                    validator: (value) {
                                      if (RegExp(
                                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                          .hasMatch(value!)) {
                                        if (errorMessage ==
                                            "akun dengan email ini sudah ada") {
                                          errorMessage = "safe";
                                          return "akun dengan email ini sudah ada";
                                        }
                                        return null;
                                      } else {
                                        return "Tolong masukkan email yang benar";
                                      }
                                    },
                                    onChanged: (value) => {
                                      setState(() {
                                        email = value;
                                      })
                                    },
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.0)),
                                        isDense: true),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ),
                                // Spacer(flex: 1),
                                Container(
                                  margin:
                                      EdgeInsets.only(top: leftComponentMargin),
                                  child: Text(
                                    "Tanggal Lahir",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                ),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: (leftComponentWidth - 60) / 4,
                                      // height: leftComponentHeight,
                                      child: TextFormField(
                                        controller: dayController,
                                        keyboardType: TextInputType.number,
                                        inputFormatters: <TextInputFormatter>[
                                          FilteringTextInputFormatter.digitsOnly
                                        ],
                                        validator: (value) {
                                          if (validateDate(
                                              dayController.text,
                                              monthController.text,
                                              yearController.text)) {
                                            return null;
                                          } else {
                                            return "";
                                          }
                                        },
                                        decoration: InputDecoration(
                                            labelText: "DD",
                                            labelStyle: Theme.of(context)
                                                .textTheme
                                                .bodyText2,
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8.0),
                                            ),
                                            // errorText: "Invalid Day",
                                            // errorBorder:
                                            //     OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                                            isDense: true),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                    SizedBox(
                                        width: (leftComponentWidth - 60) / 8,
                                        // height: leftComponentHeight,
                                        child: Center(child: Text("/"))),
                                    SizedBox(
                                      width: (leftComponentWidth - 60) / 4,
                                      // height: leftComponentHeight,
                                      child: TextFormField(
                                        controller: monthController,
                                        keyboardType: TextInputType.number,
                                        inputFormatters: <TextInputFormatter>[
                                          FilteringTextInputFormatter.digitsOnly
                                        ],
                                        validator: (value) {
                                          if (validateDate(
                                              dayController.text,
                                              monthController.text,
                                              yearController.text)) {
                                            return null;
                                          } else {
                                            return "Tanggal salah";
                                          }
                                        },
                                        decoration: InputDecoration(
                                            labelText: "MM",
                                            labelStyle: Theme.of(context)
                                                .textTheme
                                                .bodyText2,
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8.0)),
                                            isDense: true),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                    SizedBox(
                                        width: (leftComponentWidth - 60) / 8,
                                        // height: leftComponentHeight,
                                        child: Center(child: Text("/"))),
                                    SizedBox(
                                      width: (leftComponentWidth - 60) / 4,
                                      // height: leftComponentHeight,
                                      child: TextFormField(
                                        controller: yearController,
                                        keyboardType: TextInputType.number,
                                        inputFormatters: <TextInputFormatter>[
                                          FilteringTextInputFormatter.digitsOnly
                                        ],
                                        validator: (value) {
                                          if (validateDate(
                                              dayController.text,
                                              monthController.text,
                                              yearController.text)) {
                                            return null;
                                          } else {
                                            return "";
                                          }
                                        },
                                        decoration: InputDecoration(
                                            labelText: "YYYY",
                                            labelStyle: Theme.of(context)
                                                .textTheme
                                                .bodyText2,
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8.0)),
                                            isDense: true),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                    SizedBox(
                                        width: 60,
                                        // height: leftComponentHeight,
                                        child: Center(
                                          child: IconButton(
                                            icon: Icon(Icons.date_range),
                                            onPressed: () {
                                              openDatePicker(context);
                                            },
                                          ),
                                        ))
                                  ],
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(top: leftComponentMargin),
                                  child: Text(
                                    "Username",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                ),
                                SizedBox(
                                  width: leftComponentWidth,
                                  // height: leftComponentHeight,
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value!.length <= 2) {
                                        return "Username harus lebih dari 2 huruf";
                                      } else if (errorMessage ==
                                          "username tidak tersedia") {
                                        errorMessage = "safe";
                                        return "username tidak tersedia";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onChanged: (value) => {
                                      setState(() {
                                        username = value;
                                      })
                                    },
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.0)),
                                        isDense: true),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(top: leftComponentMargin),
                                  child: Text(
                                    "Password",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                ),
                                SizedBox(
                                  width: leftComponentWidth,
                                  // height: leftComponentHeight,
                                  child: TextFormField(
                                    obscureText: obscurePassword,
                                    validator: (value) {
                                      if (value != null && value.length >= 8) {
                                        return null;
                                      } else {
                                        return "Password harus terdiri dari minimal 8 karakter";
                                      }
                                    },
                                    onChanged: (value) => {
                                      setState(() {
                                        password = value;
                                      })
                                    },
                                    decoration: InputDecoration(
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            obscurePassword
                                                ? Icons.visibility_off
                                                : Icons.visibility,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              obscurePassword =
                                                  !obscurePassword;
                                            });
                                          },
                                        ),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.0)),
                                        isDense: true),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ),
                                SizedBox(
                                  width: leftComponentWidth,
                                  // height: leftComponentHeight,
                                  child: CheckboxListTile(
                                    title: Transform.translate(
                                        offset: const Offset(-10, 0),
                                        child: toa),
                                    value: acceptedController,
                                    contentPadding: EdgeInsets.zero,
                                    onChanged: (bool? value) {
                                      setState(() {
                                        acceptedController = value!;
                                      });
                                    },
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                  ),
                                ),
                                Text(
                                  (accepted == null) || (accepted!)
                                      ? ""
                                      : "Mohon baca dan setujui aturan penggunaan",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .merge(
                                        TextStyle(
                                            color: Colors.red, fontSize: 12),
                                      ),
                                )
                              ]),
                        )),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: size.height / 25),
                    width: size.width * 7 / 20,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Image.asset(
                          "assets/images/login_image.png",
                          width:
                              size.width > 1000 ? 1000 / 3 : (size.width) / 3,
                          // fit: BoxFit.contain,
                        ),
                      ],
                    ),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
          ),
          Container(
            margin: EdgeInsets.only(left: leftComponentWidth / 3, top: 20),
            child: ElevatedButton(
              onPressed: () async {
                if (!_formKey.currentState!.validate() &&
                    errorMessage != "safe") {
                  return;
                }
                errorMessage = null;
                String birthDate = yearController.text.padLeft(4, '0') +
                    "-" +
                    monthController.text.padLeft(2, '0') +
                    "-" +
                    dayController.text.padLeft(2, '0');

                setState(() {
                  accepted = acceptedController;
                });

                if (acceptedController && _formKey.currentState!.validate()) {
                  final response = await signUp(
                    username!,
                    password!,
                    namaLengkap!,
                    email,
                    birthDate,
                    ref,
                  );

                  setState(() {
                    errorMessage = response.message;
                    data = response.data;
                  });

                  if (response.code == 200) {
                    // Verification Popup
                    Navigator.of(context).push(PopUpDialogRoute(
                        barrierDismissible: false,
                        builder: (context) => Center(
                              child: SizedBox(
                                height: 300,
                                width: 500,
                                child: VerificationPopUp(),
                              ),
                            )));
                  }

                  _formKey.currentState!.validate();
                }
              },
              child: Text(
                "Daftar",
                style: Theme.of(context).textTheme.button,
              ),
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                  shadowColor: Color.fromARGB(0, 0, 0, 0),
                  primary: Theme.of(context).splashColor,
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(7.0),
                  )),
            ),
          ),
        ], crossAxisAlignment: CrossAxisAlignment.start));
  }

  bool validateDate(String? day, String? month, String? year) {
    if (day == null || month == null || year == null) {
      return false;
    } else {
      year = year.padLeft(4, '0');
      month = month.padLeft(2, '0');
      day = day.padLeft(2, '0');
      String date = year + "-" + month + "-" + day;
      DateTime datetime = DateTime.parse(date);
      DateTime now = DateTime.now();
      if (DateTime(datetime.year, datetime.month, datetime.day)
              .difference(DateTime(now.year, now.month, now.day))
              .inDays >
          0) {
        return false;
      }
      String strYear = datetime.year.toString().padLeft(4, '0');
      String strMonth = datetime.month.toString().padLeft(2, '0');
      String strDay = datetime.day.toString().padLeft(2, '0');
      String completeStrDate = strYear + "-" + strMonth + "-" + strDay;
      return date == completeStrDate;
    }
  }

  openDatePicker(BuildContext context) async {
    {
      DateTime? selected = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(1900),
          lastDate: DateTime.now());

      if (selected == null) return;

      dayController.text = selected.day.toString();
      monthController.text = selected.month.toString();
      yearController.text = selected.year.toString();
    }
  }
}
