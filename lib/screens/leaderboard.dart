import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/services/user.dart';

import '../model/user.dart';

class Leaderboard extends StatefulHookConsumerWidget {
  const Leaderboard({Key? key}) : super(key: key);

  @override
  _LeaderboardState createState() => _LeaderboardState();
}

class _LeaderboardState extends ConsumerState<Leaderboard> {
  List<User>? data;

  @override
  Widget build(BuildContext context) {
    getLeaderboard(ref).then((res) {
      if (!mounted) return;
      setState(() {
        data = res;
      });
    });

    return SingleChildScrollView(
      child: Column(children: [
        Container(
          margin: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width / 10, 0,
              MediaQuery.of(context).size.width / 10, 50),
        ),
        LeaderboardCells(users: data)
      ]),
    );
  }
}

class LeaderboardCells extends StatelessWidget {
  const LeaderboardCells({Key? key, required this.users}) : super(key: key);

  final List<User>? users;

  @override
  Widget build(BuildContext context) {
    if (users == null) {
      return Container();
    } else {
      var rank1 = RankCell(
          username: users![0].username, rank: 1, exp: users![0].experience);

      if (users!.length == 1) {
        return rank1;
      }

      var rank2 = RankCell(
          username: users![1].username, rank: 2, exp: users![1].experience);

      if (users!.length == 2) {
        return Column(
          children: [rank1, rank2],
        );
      }

      var rank3 = RankCell(
          username: users![2].username, rank: 3, exp: users![2].experience);

      if (users!.length == 3) {
        return Column(
          children: [rank1, rank2, rank3],
        );
      }

      var unrank = users!.sublist(3);

      List<Widget> total = unrank.map<Widget>((e) {
        return UnRankCell(username: e.username, exp: e.experience);
      }).toList();

      total = [rank1 as Widget, rank2, rank3] + total;

      return Column(children: total);
    }
  }
}

class RankCell extends StatelessWidget {
  const RankCell(
      {Key? key, required this.username, required this.rank, required this.exp})
      : super(key: key);

  final String username;
  final int exp;
  final int rank;

  @override
  Widget build(BuildContext context) {
    if (username == "") {
      return Container();
    }
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Stack(children: [
        Center(
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: 77,
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              border:
                  Border.all(color: Theme.of(context).primaryColor, width: 2),
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
          ),
        ),
        Center(
          child: Container(
            padding: const EdgeInsets.only(left: 30, right: 30),
            height: 70,
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
              color: Colors.white,
              border:
                  Border.all(color: Theme.of(context).primaryColor, width: 2),
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Row(
              children: [
                Row(
                  children: [
                    SizedBox(
                        width: 50,
                        child: Image.asset(
                            "assets/images/Rank" + rank.toString() + ".png")),
                    Container(
                      margin: const EdgeInsets.only(left: 40),
                      child: Text(
                        username,
                        style: (Theme.of(context).textTheme.button!).merge(
                            TextStyle(color: Theme.of(context).primaryColor)),
                      ),
                    ),
                  ],
                ),
                Text(
                  exp.toString() + " exp",
                  style: (Theme.of(context).textTheme.button!).merge(TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Theme.of(context).primaryColor)),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
        ),
      ]),
    );
  }
}

class UnRankCell extends StatelessWidget {
  const UnRankCell({Key? key, required this.username, required this.exp})
      : super(key: key);

  final String username;
  final int exp;

  @override
  Widget build(BuildContext context) {
    if (username == "") {
      return Container();
    }
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Stack(children: [
        Center(
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: 77,
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              border:
                  Border.all(color: Theme.of(context).primaryColor, width: 2),
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
          ),
        ),
        Center(
          child: Container(
            padding: const EdgeInsets.only(left: 30, right: 30),
            height: 70,
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
              color: Colors.white,
              border:
                  Border.all(color: Theme.of(context).primaryColor, width: 2),
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Row(
              children: [
                Text(
                  username,
                  style: (Theme.of(context).textTheme.button!)
                      .merge(TextStyle(color: Theme.of(context).primaryColor)),
                ),
                Text(
                  exp.toString() + " exp",
                  style: (Theme.of(context).textTheme.button!).merge(TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Theme.of(context).primaryColor)),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
        ),
      ]),
    );
  }
}
