import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/services/user.dart';

import '../model/user.dart';
import '../provider/user_state.dart';
import 'package:fluttertoast/fluttertoast.dart';

class UserSetting extends StatefulHookConsumerWidget {
  const UserSetting({Key? key}) : super(key: key);

  @override
  ConsumerState<UserSetting> createState() => _UserSettingState();
}

class _UserSettingState extends ConsumerState<UserSetting> {
  String? errorMessage;

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    User? _user = ref.watch(UserState.userProvider);
    double componentWidth = 600;

    final oldPasswordController = TextEditingController();
    final newPasswordController = TextEditingController();
    final newPasswordConfirmationController = TextEditingController();

    return Center(
      child: Container(
          margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width - 500) /
              2,
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Spacer(flex: 2),
                SizedBox(
                  // color: Colors.amber,
                  width: componentWidth,
                  child: Wrap(
                    spacing: 30,
                    children: [
                      SizedBox(
                        child: Text("Username",
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2!
                                .merge(TextStyle(fontSize: 18))),
                        width: 150,
                      ),
                      Text("Email",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2!
                              .merge(TextStyle(fontSize: 18))),
                    ],
                  ),
                ),
                SizedBox(
                  width: componentWidth,
                  child: Wrap(
                    direction: Axis.horizontal,
                    spacing: 30,
                    children: [
                      SizedBox(
                        child: Text(_user == null ? "" : _user.username,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .merge(TextStyle(fontSize: 20))),
                        width: 150,
                      ),
                      SizedBox(
                        child: Text(_user == null ? "" : _user.email,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .merge(TextStyle(fontSize: 20))),
                        width: 320,
                      ),
                    ],
                  ),
                ),
                Spacer(flex: 1),
                Text("Password Lama"),
                SizedBox(
                    width: componentWidth,
                    child: TextFormField(
                        obscureText: true,
                        controller: oldPasswordController,
                        validator: (value) {
                          if (oldPasswordController.text.length < 8) {
                            return "Password harus terdiri dari minimal 8 karakter";
                          } else if (errorMessage != null) {
                            oldPasswordController.text = "";
                            return "Password salah";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                            isDense: true))),
                Spacer(flex: 1),
                Text("Password Baru"),
                SizedBox(
                    width: componentWidth,
                    child: TextFormField(
                        obscureText: true,
                        validator: (value) {
                          if (newPasswordController.text.length < 8) {
                            return "Password harus terdiri dari minimal 8 karakter";
                          } else {
                            return null;
                          }
                        },
                        controller: newPasswordController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                            isDense: true))),
                Spacer(flex: 1),
                Text("Konfirmasi Password Baru"),
                SizedBox(
                    width: componentWidth,
                    child: TextFormField(
                        obscureText: true,
                        validator: (value) {
                          if (newPasswordController.text ==
                              newPasswordConfirmationController.text) {
                            return null;
                          } else {
                            return "Password konfirmasi tidak cocok";
                          }
                        },
                        controller: newPasswordConfirmationController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                            isDense: true))),
                Spacer(flex: 1),
                Center(
                  child: ElevatedButton(
                    onPressed: () async {
                      if (!_formKey.currentState!.validate()) {
                        return;
                      }

                      errorMessage = null;

                      final response = await changePassword(
                          _user!.username,
                          oldPasswordController.text,
                          newPasswordController.text,
                          ref);

                      if (response.code == 200) {
                        oldPasswordController.text = "";
                        newPasswordController.text = "";
                        newPasswordConfirmationController.text = "";

                        Fluttertoast.showToast(
                            msg: "Password baru berhasil disimpan",
                            toastLength: Toast.LENGTH_LONG,
                            fontSize: 20,
                            backgroundColor: Theme.of(context).primaryColor,
                            textColor: Colors.white,
                            webPosition: 'center',
                            webBgColor: "#5840BA");
                      } else {
                        errorMessage = response.message;
                        _formKey.currentState!.validate();
                      }
                    },
                    child: Text(
                      "Simpan",
                      style: Theme.of(context).textTheme.button,
                    ),
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                        shadowColor: Color.fromARGB(0, 0, 0, 0),
                        primary: Theme.of(context).splashColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7.0),
                        )),
                  ),
                ),
                Spacer(flex: 5),
              ],
            ),
          )),
    );
  }
}
