import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/model/user.dart';
import 'package:iqra/provider/lang_state.dart';
import 'package:iqra/provider/user_state.dart';
import 'package:iqra/widgets/menu_item.dart';
import 'package:tuple/tuple.dart';

class DashboardScreen extends ConsumerWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Tuple2<String, String> pairLang = ref.watch(LangState.langProvider);
    final String natLang = pairLang.item1;
    final String targetLang = pairLang.item2;
    final User? user = ref.read(UserState.userProvider);

    final String introContent = natLang == LanguageOptions.korea
        ? "안녕하세요!"
        : natLang == LanguageOptions.indonesia
            ? "Halo!"
            : "Hello!";

    final String scriptMenuTopContent = targetLang == LanguageOptions.indonesia
        ? "Aksara Korea"
        : "Korean Script";

    final String dictionaryMenuTopContent =
        targetLang == LanguageOptions.indonesia ? "Baca Kamus" : "Dictionary";

    final String dictionaryMenuContent =
        natLang == LanguageOptions.korea ? "한글 Words" : "Words";

    final String quizMenuTopContent =
        targetLang == LanguageOptions.indonesia ? "Latihan" : "Practice";

    final String quizMenuContent =
        natLang == LanguageOptions.korea ? "한글 Quiz" : "Quiz";

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      introContent,
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    Tooltip(
                      message: "Halo",
                      child: Icon(
                        Icons.help_outline_sharp,
                        color: Colors.grey,
                        size: 30,
                      ),
                    )
                  ],
                ),
                Text(
                  user!.fullName,
                  style: Theme.of(context).textTheme.headline2!.merge(
                        TextStyle(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                )
              ],
            ),
            Image.asset(
              "/images/dashboard.png",
              width: 338,
              height: 265,
            )
          ],
        ),
        SizedBox(
          height: 32,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              if (natLang == LanguageOptions.korea)
                MenuItem(
                  onPressed: (context) {
                    Navigator.pushNamed(context, '/aksara-korea');
                  },
                  topText: scriptMenuTopContent,
                  backgroundColor: Theme.of(context).splashColor,
                  content: "한글",
                ),
              MenuItem(
                onPressed: (context) {
                  Navigator.pushNamed(context, '/kamus');
                },
                topText: dictionaryMenuTopContent,
                backgroundColor: Theme.of(context).hintColor,
                content: dictionaryMenuContent,
              ),
              MenuItem(
                onPressed: (context) {
                  Navigator.pushNamed(context, '/flashcard');
                },
                topText: quizMenuTopContent,
                backgroundColor: Theme.of(context).splashColor,
                content: quizMenuContent,
              ),
            ],
          ),
        )
      ],
    );
  }
}
