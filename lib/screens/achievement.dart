import 'dart:math';

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../model/user.dart';
import '../provider/user_state.dart';

class Achievement extends ConsumerWidget {
  const Achievement({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    double width = MediaQuery.of(context).size.width;
    int crossAxisCount =
        (width / 250).floor() > 4 ? 4 : (max((width / 250).floor(), 2));
    double horizontalMargin = 100;
    double badgeSpacing = 20;

    final User? _user = ref.watch(UserState.userProvider);
    int wordLearned = _user == null ? 0 : _user.wordLearned;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: horizontalMargin),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        GridView(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: crossAxisCount,
              mainAxisSpacing: badgeSpacing,
              crossAxisSpacing: badgeSpacing,
            ),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            children: [
              badge("/images/Badges/Badge1.png", wordLearned >= 20, 20),
              badge("/images/Badges/Badge2.png", wordLearned >= 40, 40),
              badge("/images/Badges/Badge3.png", wordLearned >= 60, 60),
              badge("/images/Badges/Badge4.png", wordLearned >= 80, 80),
            ]),
      ]),
    );
  }

  static Widget badge(String imagePath, bool requirement, int reqAmount) {
    return Tooltip(
      child: Container(
        child: Image.asset(imagePath),
        foregroundDecoration: requirement
            ? null
            : BoxDecoration(
                color: Colors.grey, backgroundBlendMode: BlendMode.saturation),
      ),
      message: requirement
          ? toolTipTextGot(reqAmount)
          : toolTipTextHaveNotGot(reqAmount),
      preferBelow: true,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
    );
  }

  static String toolTipTextGot(int requirement) {
    return "Badge ini kamu dapatkan setelah mempelajari " +
        requirement.toString() +
        " kata baru";
  }

  static String toolTipTextHaveNotGot(int requirement) {
    return "Dapatkan badge ini dengan mempelajari " +
        requirement.toString() +
        " kata baru";
  }
}
