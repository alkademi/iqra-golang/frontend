import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:iqra/screens/login.dart';

class LandingSliderScreen extends StatelessWidget {
  const LandingSliderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    final pad = MediaQuery.of(context).size.width * 0.135;
    // Determine if we should use mobile layout or not, 600 here is
    // a common breakpoint for a typical 7-inch tablet.
    final bool isMobile = shortestSide < 600;

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(
          vertical: isMobile ? 2 * pad : 0.65 * pad,
          horizontal: pad,
        ),
        child: Center(
          child: IntroductionScreen(
            pages: [
              createFirstLanding(),
              createSecondLanding(),
              createThirdLanding(context)
            ],
            showDoneButton: false,
            showNextButton: false,
            dotsDecorator: const DotsDecorator(
              activeColor: Color(0xFFC4C4C4),
              color: Color(0xFFE9E9E9),
            ),
          ),
        ),
      ),
    );
  }

  PageViewModel createThirdLanding(BuildContext context) {
    return PageViewModel(
      title: "Ayo daftar sekarang!",
      bodyWidget: Column(
        children: [
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/register');
            },
            child: const Padding(
              padding: EdgeInsets.symmetric(
                vertical: 4,
                horizontal: 20,
              ),
              child: Text("Daftar"),
            ),
            style: ElevatedButton.styleFrom(
              primary: Theme.of(context).splashColor, // background
            ),
          ),
          const SizedBox(height: 20),
          RichText(
            text: TextSpan(
              style: Theme.of(context).textTheme.bodyText1,
              children: [
                const TextSpan(text: "Sudah punya akun? "),
                TextSpan(
                  text: "Masuk",
                  style: Theme.of(context).textTheme.overline,
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.pushNamed(context, '/login');
                    },
                ),
              ],
            ),
          ),
        ],
      ),
      image: Padding(
        padding: const EdgeInsets.only(bottom: 24),
        child: Image.asset("assets/images/landing_3.png"),
      ),
    );
  }

  PageViewModel createSecondLanding() {
    return PageViewModel(
      title: "Lihat perkembangan orang lain di seluruh dunia!",
      body:
          "Tak hanya untuk membaca kamus, Iqra juga bisa digunakan untuk berlatih kemampuan bahasa kamu",
      image: Padding(
        padding: const EdgeInsets.only(bottom: 24),
        child: Image.asset("assets/images/landing_2.png"),
      ),
    );
  }

  PageViewModel createFirstLanding() {
    return PageViewModel(
      title: "Aplikasi Kamus yang serba bisa!",
      body:
          "Tak hanya untuk membaca kamus, Iqra juga bisa digunakan untuk berlatih kemampuan bahasa kamu",
      image: Padding(
        padding: const EdgeInsets.only(bottom: 24),
        child: Image.asset("assets/images/landing_1.png"),
      ),
    );
  }
}
