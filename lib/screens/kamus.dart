// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iqra/model/dictionary.dart';
import 'package:iqra/services/dictionary.dart';

// final List<String> wordList = KamusResult.getKamus().words;
// final List<String> meanList = <String>["mean1 epicdayo 1234567890 woooooyeababy qwertyuiopasdfghjklzxcvbnm,", "mean2 epicdayo 1234567890 woooooyeababy qwertyuiopasdfghjklzxcvbnm,","mean3 epicdayo 1234567890 woooooyeababy qwertyuiopasdfghjklzxcvbnm,","mean4 epicdayo 1234567890 woooooyeababy qwertyuiopasdfghjklzxcvbnm,","word1", "word2","word3","word4","word1", "word2","word3","word4"];

class Ka extends StatefulWidget {
  const Ka({Key? key}) : super(key: key);

  @override
  _Ka createState() => _Ka();
}

class _Ka extends State<Ka> {
  TextEditingController con = TextEditingController();
  TextEditingController con1 = TextEditingController();
  late Future<KamusResult> futureKamus;
  String query = "";
  int page = 0;
  @override
  void initState() {
    futureKamus = KamusResult.getKamus("", page);
    super.initState();
    con1.text = page.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
            flex: 3,
            child: Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width / 5,
                    right: MediaQuery.of(context).size.width / 5,
                    top: 40),
                child: TextField(
                  onChanged: (value) {
                    setState(() {
                      query = value;
                      futureKamus = KamusResult.getKamus(value, page);
                    });
                  },
                  controller: con,
                  decoration: InputDecoration(
                      suffixIcon: Padding(
                          padding: EdgeInsets.only(right: 20),
                          child: Icon(Icons.search)),
                      contentPadding: EdgeInsets.only(left: 20),
                      hintText: "Cari kata dalam kamus...",
                      hintStyle: Theme.of(context).textTheme.bodyText2,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0))),
                ))),
        Expanded(
            flex: 9,
            child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width > 900
                      ? (MediaQuery.of(context).size.width - 800) / 2
                      : 50),
              child: FutureBuilder<KamusResult>(
                future: futureKamus,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data!.data == null) {
                      return Center(
                        child: Text("No Matching"),
                      );
                    }
                    return EntryList(snapshot.data!.data!.nativeWord,
                        snapshot.data!.data!.targetDefinition);
                  }
                  if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return Center(child: Text("Loading..."));
                },
              ),
            )),
        Expanded(
            flex: 2,
            child: query != ""
                ? Container()
                : Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width / 6,
                        right: MediaQuery.of(context).size.width / 6),
                    // width: MediaQuery.of(context).size.width/5,
                    // height: MediaQuery.of(context).size.height/5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextButton(
                          onPressed: () {
                            if (page > 0) {
                              setState(() {
                                page -= 1;
                                // con1.text = page.toString();
                                futureKamus =
                                    KamusResult.getKamus("", page * 10);
                              });
                            }
                          },
                          child: Text("<< Previous"),
                        ),
                        Container(
                            // width: 100,
                            child: page == 0
                                ? Wrap(
                                    spacing: 10,
                                    children: [
                                      ElevatedButton(
                                        onPressed: () {},
                                        child: Text("${page + 1}"),
                                      ),
                                      OutlinedButton(
                                        onPressed: () {
                                          setState(() {
                                            page = page + 1;
                                            futureKamus = KamusResult.getKamus(
                                                "", page * 10);
                                          });
                                        },
                                        child: Text("${page + 2}"),
                                        style: OutlinedButton.styleFrom(
                                          side: BorderSide(
                                              width: 2.0,
                                              color: Theme.of(context)
                                                  .primaryColor),
                                        ),
                                      ),
                                      Text(
                                        "...",
                                        style: TextStyle(
                                            color: Color(0xff5840ba),
                                            fontSize: 30),
                                      ),
                                      OutlinedButton(
                                        style: OutlinedButton.styleFrom(
                                          side: BorderSide(
                                              width: 2.0,
                                              color: Theme.of(context)
                                                  .primaryColor),
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            page = page + 9;
                                            futureKamus = KamusResult.getKamus(
                                                "", page * 10);
                                          });
                                        },
                                        child: Text("${page + 10}"),
                                      ),
                                    ],
                                  )
                                : Wrap(
                                    spacing: 10,
                                    children: [
                                      ButtonTheme(
                                          height: 30,
                                          minWidth: 30,
                                          child: OutlinedButton(
                                            style: OutlinedButton.styleFrom(
                                              side: BorderSide(
                                                  width: 2.0,
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                page -= 1;
                                                futureKamus =
                                                    KamusResult.getKamus(
                                                        "", page * 10);
                                              });
                                            },
                                            child: Text("$page"),
                                          )),
                                      ElevatedButton(
                                        onPressed: () {},
                                        child: Text("${page + 1}"),
                                      ),
                                      OutlinedButton(
                                        style: OutlinedButton.styleFrom(
                                          side: BorderSide(
                                              width: 2.0,
                                              color: Theme.of(context)
                                                  .primaryColor),
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            page += 1;
                                            futureKamus = KamusResult.getKamus(
                                                "", page * 10);
                                          });
                                        },
                                        child: Text("${page + 2}"),
                                      ),
                                      Text(
                                        "...",
                                        style: TextStyle(
                                            color: Color(0xff5840ba),
                                            fontSize: 30),
                                      ),
                                      OutlinedButton(
                                        style: OutlinedButton.styleFrom(
                                          side: BorderSide(
                                              width: 2.0,
                                              color: Theme.of(context)
                                                  .primaryColor),
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            page += 9;
                                            futureKamus = KamusResult.getKamus(
                                                "", page * 10);
                                          });
                                        },
                                        child: Text("${page + 10}"),
                                      ),
                                    ],
                                  )
                            // child:TextField(
                            //     textAlign: TextAlign.center,
                            //     keyboardType: TextInputType.number,
                            //     inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                            //     onChanged: (value){
                            //       page = int.parse(value) > 0 ? int.parse(value) : 0;
                            //       setState(() {
                            //         futureKamus = KamusResult.getKamus(con.text, page);
                            //       });
                            //   },
                            //   controller: con1,
                            //   decoration: InputDecoration(
                            //     hintStyle: Theme.of(context).textTheme.bodyText1,
                            //     border: OutlineInputBorder()
                            //   ),
                            // )
                            ),
                        TextButton(
                          onPressed: () {
                            setState(() {
                              page += 1;
                              // con1.text = page.toString();
                              futureKamus = KamusResult.getKamus("", page * 10);
                            });
                            print(page);
                          },
                          child: Text("Next >>"),
                        ),
                      ],
                    )))
      ],
    );
  }
}

// class Entry extends StatelessWidget {

//   const Entry({Key? key}) : super(key: key);

//   final String word = "P";
//   final String means = "ppppp";
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//           children: [
//             Text(word),
//             Text(means)
//           ],
//         );
//   }
// }

class EntryList extends StatelessWidget {
  const EntryList(this.filteredWord, this.filteredMean, {Key? key})
      : super(key: key);

  final List<String> filteredWord;
  final List<String> filteredMean;
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 30,
          mainAxisSpacing: MediaQuery.of(context).size.width > 600 ? 20 : 60,
          childAspectRatio: 2.8),
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      padding: EdgeInsets.all(8),
      itemCount: filteredWord.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
            dense: true,
            title: Text(filteredWord[index],
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .merge(TextStyle(fontSize: 28))),
            subtitle: Text(
              filteredMean[index],
              style: TextStyle(color: Colors.black, fontSize: 14),
            ));
      },
    );
  }
}

// class Buttons extends StatelessWidget {
//   const Buttons(this.page, {Key? key}) : super(key: key);

//   final int page;
//   @override
//   Widget build(BuildContext context) {
//     switch (page) {
//       case 0:
        
//         break;
//       default:
//     }
//     return Container(
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: [
//           TextButton(
//             onPressed: (){

//             }, 
//             child: Text("<< Previous"),
//           ),
//           Container(
//             width: 100,
//             child: Row(
//               children: [
//                 ElevatedButton(
//                   onPressed: (){

//                   }, 
//                   child: Text("1"),
//                 ),
//                 OutlinedButton(
//                   onPressed: (){

//                   }, 
//                   child: Text("2"),
//                 ),
//                 Text("..."),
//                 OutlinedButton(
//                   onPressed: (){

//                   }, 
//                   child: Text("10"),
//                 ),
//               ],
//             )
//           ),
//           TextButton(
//             onPressed: (){

//             }, 
//             child: Text("Next >>"),
//           ),
//         ],
//       )
//     );
//   }
// }