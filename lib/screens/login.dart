// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String username = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(40, 0, 40, 0),
      child: Column(
        children: <Widget>[
          Spacer(
            flex: 3,
          ),
          Row(children: [
            Image.asset(
              "assets/images/icon.png",
              width: 44,
            ),
            Container(
                margin: EdgeInsets.only(left: 18),
                child: Text(
                  "Masuk",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                      fontFamily: "Poppins",
                      color: Colors.black,
                      decoration: TextDecoration.none),
                ))
          ]),
          Spacer(flex: 7),
          Text("Username", style: Theme.of(context).textTheme.bodyText2),
          TextField(
            onChanged: (value) => {
              setState(() {
                username = value;
              })
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                isDense: true),
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Spacer(flex: 1),
          Text(
            "Password",
            style: Theme.of(context).textTheme.bodyText2,
          ),
          TextField(
            onChanged: (value) => {
              setState(() {
                password = value;
              })
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                isDense: true),
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Spacer(flex: 4),
          Center(
              child: SizedBox(
            width: 127,
            height: 45,
            child: ElevatedButton(
              onPressed: () {
                // Navigator.pushNamed(context, '/login');
              },
              child: Text(
                "Masuk",
                style: Theme.of(context).textTheme.button,
              ),
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).splashColor,
                  shadowColor: Color.fromARGB(0, 0, 0, 0),
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(7.0),
                  )),
            ),
          )),
          Spacer(
            flex: 2,
          ),
          Stack(
            children: [
              Divider(
                thickness: 1,
                color: Color.fromRGBO(172, 172, 172, 1),
              ),
              Center(
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  color: Colors.white,
                  child: Text(
                    "Belum punya akun?",
                    style: TextStyle(color: Color.fromRGBO(172, 172, 172, 1)),
                  ),
                ),
              )
            ],
          ),
          Spacer(
            flex: 2,
          ),
          Center(
              child: SizedBox(
            width: 127,
            height: 45,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/register');
              },
              child: Text(
                "Daftar",
                style: Theme.of(context).textTheme.button,
              ),
              style: ElevatedButton.styleFrom(
                primary: Theme.of(context).splashColor,
                shadowColor: Color.fromARGB(0, 0, 0, 0),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(7.0),
                ),
              ),
            ),
          )),
          Spacer(flex: 10),
        ],
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }
}
