// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/screens/achievement.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '../model/user.dart';
import '../provider/user_state.dart';

class ProfileScreen extends HookConsumerWidget {
  // final List<int> pencapaian;
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final User? _user = ref.watch(UserState.userProvider);
    final String? nama = _user?.username;
    final int? exp = _user?.experience;
    final int? level = exp! ~/ 100;
    return Container(
      margin: EdgeInsets.all(30.0),
      padding: EdgeInsets.all(30.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              decoration: BoxDecoration(
                  color: Color(0xff5840ba),
                  border: Border.all(
                    color: Color(0xff5840ba),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              padding: EdgeInsets.symmetric(vertical: 50.0),
              child: Container(
                margin: EdgeInsets.only(right: 100.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    // air.png jadi placeholder
                    Image.asset('assets/images/placeholder.png'),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 20.0),
                          child: Row(
                            textBaseline: TextBaseline.alphabetic,
                            crossAxisAlignment: CrossAxisAlignment.baseline,
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(right: 45.0),
                                  child: Text(
                                    nama!,
                                    style: TextStyle(
                                      fontSize: 36.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  )),
                              
                            ],
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.alphabetic,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(exp.toString(),
                              style: TextStyle(fontSize: 45, color: Colors.white),
                            ),
                            Text(" exp", style: TextStyle(fontSize: 26, color: Colors.white)),
                            Text("          "),
                            Text(_user!.wordLearned.toString(),
                              style: TextStyle(fontSize: 45, color: Colors.white),
                            ),
                            Text(" word(s) learned", style: TextStyle(fontSize: 26, color: Colors.white))
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              )),
          TextButton(
            onPressed: () {
              Navigator.pushNamed(context, '/pencapaian');
            },
            child: Text(
              "Pencapaian Saya >",
            ),
          ),
          Container(
            // margin: EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Achievement.badge(
                    "/images/Badges/Badge1.png", _user.wordLearned >= 20, 20),
                Achievement.badge(
                    "/images/Badges/Badge2.png", _user.wordLearned >= 40, 40),
                Achievement.badge(
                    "/images/Badges/Badge3.png", _user.wordLearned >= 60, 60),
                Achievement.badge(
                    "/images/Badges/Badge4.png", _user.wordLearned >= 80, 80),
              ],
            ),
          )
        ],
      ),
    );
  }
}
