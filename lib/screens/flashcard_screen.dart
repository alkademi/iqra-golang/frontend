import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/model/dictionary.dart';
import 'package:iqra/model/user.dart';
import 'package:iqra/provider/auth_state.dart';
import 'package:iqra/provider/lang_state.dart';
import 'package:iqra/provider/user_state.dart';
import 'package:iqra/services/dictionary.dart';
import 'package:iqra/widgets/flashcard.dart';
import 'package:tcard/tcard.dart';
import 'package:tuple/tuple.dart';

class FlashcardScreen extends ConsumerStatefulWidget {
  const FlashcardScreen({Key? key}) : super(key: key);

  @override
  ConsumerState<FlashcardScreen> createState() => _FlashcardScreenState();
}

class _FlashcardScreenState extends ConsumerState<FlashcardScreen> {
  final ValueNotifier<bool> _showTextInfo = ValueNotifier(true);
  final ValueNotifier<Tuple3<bool, int, int>> _showTextCongrats = ValueNotifier(
    Tuple3(false, 0, 0),
  );

  List<Widget> generateFlashcards(List<FlashcardWord> words) {
    List<Flashcard> res = [SpecialFlashcard()];

    res = [
      ...res,
      ...List.generate(
        words.length,
        (index) => Flashcard(
          key: Key('flip$index'),
          word: words[index],
        ),
      )
    ];

    return res;
  }

  @override
  Widget build(BuildContext context) {
    final User? user = ref.read(UserState.userProvider);
    final String jwt = ref.read(AuthState.jwtProvider);
    final Tuple2<String, String> pairLang = ref.watch(LangState.langProvider);
    final Future<List<FlashcardWord>> words =
        getRandomFlashcard(user!, pairLang.item1, pairLang.item2);

    return FutureBuilder(
      future: words,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          List<FlashcardWord> flashcardWords =
              snapshot.data as List<FlashcardWord>;
          var cards = generateFlashcards(flashcardWords);

          return Center(
            child: Column(
              children: [
                Text(
                  "Flashcard",
                  style: Theme.of(context).textTheme.headline4!.merge(
                        TextStyle(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                ),
                ValueListenableBuilder(
                  valueListenable: _showTextCongrats,
                  builder: (context, Tuple3 val, child) {
                    if (val.item1) {
                      return SizedBox(
                        width: 1024,
                        height: 500,
                        child: Center(
                          child: Text(
                            "SELAMAT!\n Kamu telah menyelesaikan flashcard sesi ini!\nKamu berhasil menguasai ${val.item2} / 10 kata random!\nKamu mendapatkan ${val.item3} exp!",
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline4!.merge(
                                  TextStyle(
                                    color: Theme.of(context).primaryColor,
                                  ),
                                ),
                          ),
                        ),
                      );
                    }
                    return Container();
                  },
                ),
                ValueListenableBuilder(
                    valueListenable: _showTextCongrats,
                    builder: (context, Tuple3 val, child) {
                      if (!val.item1) {
                        return TCard(
                          cards: cards,
                          size: Size(360, 500),
                          onEnd: () {
                            _showTextCongrats.value = _showTextCongrats.value.withItem1(true);
                          },
                          onForward: (index, info) {
                            if (index == 1) {
                              _showTextInfo.value = false;
                              return;
                            }

                            if (info.direction == SwipDirection.Right) {
                            _showTextCongrats.value = _showTextCongrats.value.withItem2(val.item2 + 1);
                            _showTextCongrats.value = _showTextCongrats.value.withItem3(val.item3 + 10);
                              addWordLearned(
                                  user.username,
                                  flashcardWords.elementAt(index - 2).id,
                                  pairLang.item1,
                                  pairLang.item2,
                                  jwt);
                            }
                          },
                        );
                      }
                      return Container();
                    }),
                ValueListenableBuilder(
                  valueListenable: _showTextInfo,
                  builder: (context, bool val, child) {
                    if (val) {
                      return Text(
                        "Sentuh kartu untuk memulai!",
                        style: Theme.of(context).textTheme.headline5,
                      );
                    }
                    return Container();
                  },
                )
              ],
            ),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
