// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';
import 'package:iqra/screens/login.dart';

class SignUp extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool accepted = false;
  String namaLengkap = "";
  String email = "";
  String username = "";
  String password = "";

  var toa = RichText(
      text: TextSpan(
    style: TextStyle(
        fontSize: 14,
        fontFamily: "Poppins",
        color: Color.fromRGBO(172, 172, 172, 1)),
    children: const <TextSpan>[
      TextSpan(text: 'Setuju dengan '),
      TextSpan(
          text: 'terms and condition',
          style: TextStyle(
              color: Color.fromRGBO(88, 64, 186, 1),
              decoration: TextDecoration.underline)),
      TextSpan(text: ' iqra')
    ],
  ));

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(40, 0, 40, 0),
      child: Column(
        children: <Widget>[
          Spacer(
            flex: 5,
          ),
          Row(children: [
            Image.asset(
              "assets/images/icon.png",
              width: 44,
            ),
            Container(
              margin: EdgeInsets.only(left: 18),
              child: Text("Daftar",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                      fontFamily: "Poppins",
                      color: Colors.black,
                      decoration: TextDecoration.none)),
            )
          ]),
          Spacer(flex: 5),
          Text(
            "Nama Lengkap",
            style: Theme.of(context).textTheme.bodyText2,
          ),
          TextField(
            onChanged: (value) => {
              setState(() {
                namaLengkap = value;
              })
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                isDense: true),
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Spacer(flex: 1),
          Text(
            "Email",
            style: Theme.of(context).textTheme.bodyText2,
          ),
          TextField(
            onChanged: (value) => {
              setState(() {
                email = value;
              })
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                isDense: true),
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Spacer(flex: 1),
          Text(
            "Username",
            style: Theme.of(context).textTheme.bodyText2,
          ),
          TextField(
            onChanged: (value) => {
              setState(() {
                username = value;
              })
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                isDense: true),
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Spacer(flex: 1),
          Text(
            "Password",
            style: TextStyle(
                color: Color.fromRGBO(172, 172, 172, 1),
                fontFamily: "Poppins",
                fontWeight: FontWeight.w400,
                fontSize: 14),
          ),
          TextField(
            onChanged: (value) => {
              setState(() {
                password = value;
              })
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                isDense: true),
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Spacer(flex: 1),
          CheckboxListTile(
            title:
                Transform.translate(offset: const Offset(-10, 0), child: toa),
            value: accepted,
            contentPadding: EdgeInsets.zero,
            onChanged: (bool? value) {
              setState(() {
                accepted = value!;
              });
            },
            controlAffinity: ListTileControlAffinity.leading,
          ),
          Spacer(flex: 2),
          Center(
              child: SizedBox(
            width: 127,
            height: 45,
            child: ElevatedButton(
              onPressed: () {
                // Navigator.pushNamed(context, '/register');
              },
              child: Text(
                "Daftar",
                style: Theme.of(context).textTheme.button,
              ),
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).splashColor,
                  shadowColor: Color.fromARGB(0, 0, 0, 0),
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(7.0),
                  )),
            ),
          )),
          Spacer(
            flex: 2,
          ),
          Stack(
            children: [
              Divider(
                thickness: 1,
                color: Color.fromRGBO(172, 172, 172, 1),
              ),
              Center(
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  color: Colors.white,
                  child: Text(
                    "Sudah punya akun?",
                    style: TextStyle(color: Color.fromRGBO(172, 172, 172, 1)),
                  ),
                ),
              )
            ],
          ),
          Spacer(
            flex: 2,
          ),
          Center(
              child: SizedBox(
            width: 127,
            height: 45,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/login');
              },
              child: Text(
                "Masuk",
                style: Theme.of(context).textTheme.button,
              ),
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).splashColor,
                  shadowColor: Color.fromARGB(0, 0, 0, 0),
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(7.0),
                  )),
            ),
          )),
          Spacer(flex: 10),
        ],
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }
}
