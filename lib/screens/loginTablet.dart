// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';
import 'package:iqra/services/user.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:restart_app/restart_app.dart';

import '../widgets/forgot_password_popup.dart';
import '../widgets/popup_dialog_route.dart';

class LoginTablet extends ConsumerStatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  LoginTablet({Key? key}) : super(key: key);

  @override
  ConsumerState<LoginTablet> createState() => _LoginTabletState();
}

class _LoginTabletState extends ConsumerState<LoginTablet> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String? errorMessage;

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var leftComponentWidth =
        size.width > 1000 ? 430 + 0.05 * size.width : (size.width - 40) / 2;
    return Container(
      // color: Colors.cyan,
      width: size.width,
      padding:
          EdgeInsets.fromLTRB(size.width * 3 / 40, size.height / 20, 40, 0),
      child: Column(
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Row(
              children: [
                Image.asset(
                  "assets/images/icon.png",
                  width: 44,
                ),
                Container(
                  margin: EdgeInsets.only(left: 18),
                  child: Text("Masuk",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w600,
                          fontFamily: "Poppins",
                          color: Colors.black,
                          decoration: TextDecoration.none)),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  "Belum punya akun?",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                SizedBox(
                  width: 20,
                  height: 30,
                  child: VerticalDivider(
                    thickness: 1,
                    color: Colors.black,
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/register');
                  },
                  child: Text(
                    "Daftar",
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w500),
                  ),
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.fromLTRB(12, 7, 12, 7),
                      primary: Theme.of(context).splashColor,
                      shadowColor: Color.fromARGB(0, 0, 0, 0),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(7.0),
                      )),
                ),
              ],
            )
          ]),
          Form(
            key: _formKey,
            child: Row(
              children: [
                SizedBox(
                  // color: Colors.red,
                  height: 3.5 * size.height / 5,
                  width: leftComponentWidth,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      // Spacer(flex: 1),
                      Text(
                        "Username",
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                      SizedBox(
                        width: leftComponentWidth,
                        child: TextFormField(
                          controller: usernameController,
                          validator: (value) {
                            if (value == "") {
                              errorMessage = null;
                              return "Username harus diisi";
                            } else if (errorMessage == "akun tidak ditemukan") {
                              errorMessage = "safe";
                              return "Akun tidak ditemukan";
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0)),
                              isDense: true),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      SizedBox(height: size.height / 40),
                      // Spacer(flex: 1),
                      Text(
                        "Password",
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                      SizedBox(
                        width: leftComponentWidth,
                        child: TextFormField(
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                          controller: passwordController,
                          validator: (value) {
                            if (value == null) {
                              return "Tolong isi password anda";
                            } else if (value.length < 8) {
                              return "Password harus terdiri dari minimal 8 karakter";
                            } else if (errorMessage == "password salah") {
                              errorMessage = "safe";
                              passwordController.text = "";
                              return "Password salah";
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0)),
                              isDense: true),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      SizedBox(height: size.height * 1.5 / 20),
                      Center(
                        child: ElevatedButton(
                          onPressed: () async {
                            if (!_formKey.currentState!.validate() &&
                                errorMessage != "safe") {
                              return;
                            }
                            errorMessage = null;
                            var response = await login(usernameController.text,
                                passwordController.text, ref);

                            if (response.code != 200) {
                              // Error handling
                              setState(() {
                                errorMessage = response.message;
                              });
                              _formKey.currentState!.validate();
                            } else {
                              Restart.restartApp();
                            }
                          },
                          child: Text(
                            "Masuk",
                            style: Theme.of(context).textTheme.button,
                          ),
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                              primary: Theme.of(context).splashColor,
                              shadowColor: Color.fromARGB(0, 0, 0, 0),
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(7.0),
                              )),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Center(
                              child: InkWell(
                            child: Text(
                              "Lupa password?",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .merge(TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      decoration: TextDecoration.underline)),
                            ),
                            onTap: () {
                              Navigator.of(context).push(PopUpDialogRoute(
                                  barrierDismissible: true,
                                  builder: (context) => Center(
                                        child: SizedBox(
                                          height: 325,
                                          width: 400,
                                          child: ForgotPasswordPopUp(),
                                        ),
                                      )));
                            },
                          )))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: size.height / 25),
                  height: 3.5 * size.height / 5,
                  width: size.width * 7 / 20,
                  // color: Colors.amber,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/login_image.png",
                        width: size.width > 1000 ? 1000 / 3 : (size.width) / 3,
                        // fit: BoxFit.contain,
                      ),
                    ],
                  ),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.start,
      ),
    );
  }
}
