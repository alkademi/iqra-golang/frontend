import 'dart:math';

import 'package:flutter/material.dart';

class AksaraKorea extends StatelessWidget {
  const AksaraKorea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    int crossAxisCount =
        (width / 180).floor() > 6 ? 6 : (max((width / 180).floor(), 3));
    double horizontalMargin = max(
        200 -
            (width > 1280
                ? -(width - 1280) / 2
                : (width > 1000 ? 0 : (1000 - width) / 5)),
        20);
    double aksaraSpacing = 30;

    return SingleChildScrollView(
      child: Container(
        margin:
            EdgeInsets.symmetric(vertical: 20, horizontal: horizontalMargin),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Text(
              "Huruf Konsonan",
              style: Theme.of(context).textTheme.headline6?.merge(TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.w600)),
            ),
          ),
          GridView(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: crossAxisCount,
                mainAxisSpacing: aksaraSpacing,
                crossAxisSpacing: aksaraSpacing,
              ),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: [
                Image.asset("/images/AksaraKorea/g.png"),
                Image.asset("/images/AksaraKorea/n.png"),
                Image.asset("/images/AksaraKorea/d.png"),
                Image.asset("/images/AksaraKorea/r.png"),
                Image.asset("/images/AksaraKorea/m.png"),
                Image.asset("/images/AksaraKorea/b.png"),
                Image.asset("/images/AksaraKorea/s.png"),
                Image.asset("/images/AksaraKorea/ng.png"),
                Image.asset("/images/AksaraKorea/j.png"),
                Image.asset("/images/AksaraKorea/ch.png"),
                Image.asset("/images/AksaraKorea/k.png"),
                Image.asset("/images/AksaraKorea/t.png"),
                Image.asset("/images/AksaraKorea/p.png"),
                Image.asset("/images/AksaraKorea/h.png"),
              ]),
          Container(
            margin: EdgeInsets.only(bottom: 20, top: 40),
            child: Text(
              "Huruf Vokal",
              style: Theme.of(context).textTheme.headline6?.merge(TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.w600)),
            ),
          ),
          GridView(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: crossAxisCount,
              mainAxisSpacing: aksaraSpacing,
              crossAxisSpacing: aksaraSpacing,
            ),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            children: [
              Image.asset("/images/AksaraKorea/a.png"),
              Image.asset("/images/AksaraKorea/ya.png"),
              Image.asset("/images/AksaraKorea/eo.png"),
              Image.asset("/images/AksaraKorea/yeo.png"),
              Image.asset("/images/AksaraKorea/o.png"),
              Image.asset("/images/AksaraKorea/yo.png"),
              Image.asset("/images/AksaraKorea/u.png"),
              Image.asset("/images/AksaraKorea/yu.png"),
              Image.asset("/images/AksaraKorea/eu.png"),
              Image.asset("/images/AksaraKorea/i.png"),
            ],
          )
        ]),
      ),
    );
  }
}
