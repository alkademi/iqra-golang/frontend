class Badge {
  String id;
  String name;
  String description;
  String photo;
  int expRequired;
  int wordRequired;

  Badge({
    required this.id,
    required this.name,
    required this.description,
    required this.photo,
    required this.expRequired,
    required this.wordRequired,
  });

  factory Badge.fromJson(dynamic json) {
    return Badge(
      id: json['badge_id'] as String,
      name: json['name'] as String,
      description: json['description'] as String,
      photo: json['model'] as String,
      expRequired: json['exp_req'] as int,
      wordRequired: json['word_req'] as int,
    );
  }
}