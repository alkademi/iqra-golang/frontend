class FlashcardWord {
  int id;
  String nativeWord;
  dynamic targetWord;
  String targetDefinition;
  String nativeLang;
  String targetLang;

  FlashcardWord({
    required this.id,
    required this.nativeWord,
    required this.targetWord,
    required this.targetDefinition,
    required this.nativeLang,
    required this.targetLang,
  });

  factory FlashcardWord.fromJson(Map<String, dynamic> json) {
    return FlashcardWord(
      id: json['dict_id'] as int,
      nativeWord: json['native_word'] as String,
      targetWord: json['target_word'],
      targetDefinition: json['target_definition'] as String,
      nativeLang: json['native_lang'] as String,
      targetLang: json['target_lang'] as String,
    );
  }
}

class Dictionary {
  List<int> id;
  List<String> nativeWord;
  List<String> targetWord;
  List<String> targetDefinition;
  List<String> nativeLang;
  List<String> targetLang;

  Dictionary({
    required this.id,
    required this.nativeWord,
    required this.targetWord,
    required this.targetDefinition,
    required this.nativeLang,
    required this.targetLang,
  });

  factory Dictionary.fromJson(dynamic json) {
    List<String> nativeWords, targetWords, targetDefinitions, nativeLangs, targetLangs;
    List<int> ids = <int>[];
    nativeWords = <String>[];
    targetWords = <String>[];
    targetDefinitions = <String>[];
    nativeLangs = <String>[];
    targetLangs = <String>[];
    for (var item in json) {
      print(item);
      var a = item['dict_id'];
      // print(a);
      var b = item['native_word'];
      var c = item['target_word'];
      var d = item['target_definition'];
      var e = item['native_lang'];
      var f = item['target_lang'];

      ids.add(a);
      nativeWords.add(b as String);
      // print(b);
      if (c == null){
        // print(b);
        targetWords.add("");
      }
      else{
        targetWords.add(c as String);
        
      }
      // targetWords.add(c as String);
      targetDefinitions.add(d as String);
      nativeLangs.add(e as String);
      targetLangs.add(f as String);
    }
    // final userdata = List<dynamic>.from(
    //   json.map<dynamic>(
    //     (dynamic item) => response['body'],
    //   ),
    // );
    return Dictionary(
      id: ids,
      nativeWord: nativeWords,
      targetWord: targetWords,
      targetDefinition: targetDefinitions,
      nativeLang: nativeLangs,
      targetLang: targetLangs,
    );
  }
}