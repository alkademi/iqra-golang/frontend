class GeneralResponse {
  int code;
  String message;
  dynamic data;

  GeneralResponse({
    required this.code,
    required this.message,
    this.data,
  });
}