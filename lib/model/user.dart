class User {
  String username;
  String fullName;
  String email;
  DateTime birthDate;
  int experience;
  int wordLearned;
  int rank;

  User(
      {required this.username,
      required this.fullName,
      required this.email,
      required this.birthDate,
      required this.experience,
      required this.wordLearned,
      required this.rank});

  factory User.fromJson(dynamic json) {
    // TODO: remove id or set id from backend
    return User(
        username: json['username'] ?? json['username'] as String,
        fullName: json['full_name'] ?? json['full_name'] as String,
        email: json['email'] ?? json['email'] as String,
        experience: json['exp'] as int,
        wordLearned: json['word_learned'] as int,
        birthDate: DateTime.parse(json['birth_date']),
        rank: json['rank']);
  }

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'full_name': fullName,
      'email': email,
      'exp': experience,
      'word_learned': wordLearned,
      'birth_date': birthDate.toIso8601String(),
      'rank': rank
    };
  }
}
