import 'package:flutter/material.dart';
import 'package:iqra/widgets/dashboard_right_widget.dart';

class DictionaryRightWidget extends StatelessWidget {
  const DictionaryRightWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Image.asset("/images/Language/" + nativeLanguage.value + ".png"),
        Text(
          " - ",
          style: Theme.of(context).textTheme.headline6,
        ),
        Image.asset("/images/Language/" + targetLanguage.value + ".png")
      ]),
    );
  }
}
