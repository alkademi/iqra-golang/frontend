import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  const MenuItem(
      {required this.topText,
      required this.backgroundColor,
      required this.content,
      required this.onPressed,
      Key? key})
      : super(key: key);

  final String topText;
  final String content;
  final Color backgroundColor;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onPressed(context),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 56),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              topText + " >",
              style: Theme.of(context).textTheme.headline6!.merge(
                    TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
            ),
            Container(
              width: 450,
              height: 208,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: backgroundColor,
              ),
              margin: EdgeInsets.symmetric(vertical: 16),
              // color: backgroundColor,
              alignment: Alignment.center,
              child: Text(
                content,
                style: Theme.of(context).textTheme.headline2!.merge(TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
