import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/model/user.dart';
import 'package:iqra/provider/auth_state.dart';
import 'package:iqra/provider/user_state.dart';
import 'package:iqra/services/user.dart';
import 'package:iqra/widgets/popup_dialog_route.dart';
import 'package:restart_app/restart_app.dart';

class SignUpVerification extends StatelessWidget {
  const SignUpVerification({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 450,
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                padding: EdgeInsets.fromLTRB(12, 16, 12, 16),
                shadowColor: Color.fromARGB(0, 0, 0, 0),
                primary: Theme.of(context).splashColor),
            onPressed: () {
              Navigator.of(context).push(PopUpDialogRoute(
                  barrierDismissible: false,
                  builder: (context) => Center(
                        child: SizedBox(
                          height: 300,
                          width: 500,
                          child: VerificationPopUp(),
                        ),
                      )));
            },
            child: Text(
              "Verify",
              style: Theme.of(context)
                  .textTheme
                  .button!
                  .merge(TextStyle(fontSize: 16)),
            )));
  }
}

class VerificationPopUp extends StatefulHookConsumerWidget {
  const VerificationPopUp({Key? key}) : super(key: key);

  @override
  ConsumerState<VerificationPopUp> createState() => _VerificationPopUpState();
}

class _VerificationPopUpState extends ConsumerState<VerificationPopUp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String? regisToken;
  String? errorMessage;

  @override
  Widget build(BuildContext context) {
    final User? _user = ref.watch(UserState.userProvider);
    // final User? _user = User(
    //     id: "id",
    //     email: "blabla@bla.com",
    //     fullName: "full",
    //     username: "user",
    //     birthDate: DateTime.now(),
    //     experience: 0,
    //     wordLearned: 0);

    if (_user == null) {
      WidgetsBinding.instance?.addPostFrameCallback((_) {
        Restart.restartApp();
      });
      return Material(child: Text(""));
      // return Material();
    }

    return Material(
      borderRadius: BorderRadius.circular(20),
      child: Column(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              iconSize: 15,
              splashRadius: 15,
              onPressed: () {
                Navigator.of(context).push(PopUpDialogRoute(
                    barrierDismissible: true,
                    animationDuration: 0,
                    builder: (context) => Center(
                          child: SizedBox(
                            height: 250,
                            width: 400,
                            child: DeleteAccountVerificationPopUp(),
                          ),
                        )));
              },
              icon: Icon(Icons.close),
            ),
          ),
          Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      "Token verifikasi telah dikirimkan ke " +
                          _user.email +
                          ". Masukkan token verifikasi tersebut pada tempat dibawah ini.",
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                SizedBox(
                  width: 300,
                  child: TextFormField(
                    textAlign: TextAlign.center,
                    validator: (value) {
                      if (errorMessage != "Success") {
                        return "Token salah";
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        regisToken = value;
                      });
                    },
                    decoration: InputDecoration(
                      errorStyle: TextStyle(),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () async {
                      // errorMessage = null;
                      final response = await validateAccount(
                          _user.username, regisToken, ref);

                      setState(() {
                        errorMessage = response.message;
                      });

                      if (!_formKey.currentState!.validate()) {
                        return;
                      } else {
                        Restart.restartApp();
                      }
                    },
                    child: Text("Kirim")),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class DeleteAccountVerificationPopUp extends StatefulHookConsumerWidget {
  const DeleteAccountVerificationPopUp({Key? key}) : super(key: key);

  @override
  ConsumerState<DeleteAccountVerificationPopUp> createState() =>
      _DeleteAccountVerificationPopUpState();
}

class _DeleteAccountVerificationPopUpState
    extends ConsumerState<DeleteAccountVerificationPopUp> {
  @override
  Widget build(BuildContext context) {
    final User? _user = ref.watch(UserState.userProvider);

    if (_user == null) {
      Navigator.of(context).pop();
      return Material();
    }

    return Material(
      borderRadius: BorderRadius.circular(20),
      child: Column(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              iconSize: 15,
              splashRadius: 15,
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.close),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Column(
              children: [
                Text(
                  "Keluar akan menyebabkan akun " +
                      _user.username +
                      " terhapus, apakah anda ingin melanjutkan?",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.red,
                              shadowColor: Color.fromRGBO(0, 0, 0, 0)),
                          onPressed: () {
                            // Delete Account
                            // UserState.removeUser(ref);
                            deleteAccount(_user.username, ref);
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                          },
                          child: Text("Ya")),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              shadowColor: Color.fromRGBO(0, 0, 0, 0)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text("Tidak")),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
