import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../model/user.dart';
import '../provider/user_state.dart';

class LeaderboardRightWidget extends HookConsumerWidget {
  const LeaderboardRightWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final User? _user = ref.watch(UserState.userProvider);

    return Container(
        child: Row(
      children: [
        Text(
          "Peringkat saya: ",
          style: (Theme.of(context).textTheme.button!)
              .merge(TextStyle(color: Colors.black, fontSize: 22)),
        ),
        Text(
          _user == null ? "?" : _user.rank.toString(),
          style: (Theme.of(context).textTheme.bodyText1!)
              .merge(TextStyle(fontSize: 22)),
        ),
      ],
    ));
  }
}
