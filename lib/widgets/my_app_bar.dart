import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  MyAppBar(this._scaffoldKey, this.title, this.rightWidgetAppBar, this.backgroundColor,
      {Key? key})
      :  preferredSize = Size.fromHeight(96), super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey;
  final String title;
  final Widget? rightWidgetAppBar;
  final Color? backgroundColor;

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      toolbarHeight: 96,
      backgroundColor: backgroundColor,
      elevation: 0,
      leading: null,
      leadingWidth: 0,
      title: Padding(
        padding: const EdgeInsets.all(24),
        child: Row(
          children: [
            SizedBox(
              height: 40,
              width: 40,
              child: IconButton(
                padding: const EdgeInsets.all(0.0),
                splashRadius: 32,
                icon: Image.asset(
                  "assets/icons/4dots.png",
                ),
                onPressed: () {
                  _scaffoldKey.currentState!.openDrawer();
                },
              ),
            ),
            const SizedBox(
              width: 48,
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.headline3,
            ),
            Spacer(),
            rightWidgetAppBar ?? SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
