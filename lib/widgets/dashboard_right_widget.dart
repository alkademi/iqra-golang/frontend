import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:iqra/provider/lang_state.dart';

// Languages = "eng", "indo", "kor"
final nativeLanguage = ValueNotifier<String>(LanguageOptions.english);
final targetLanguage = ValueNotifier<String>(LanguageOptions.indonesia);

class DashboardRightWidget extends StatefulWidget {
  const DashboardRightWidget({Key? key}) : super(key: key);

  @override
  State<DashboardRightWidget> createState() => _DashboardRightWidgetState();
}

class _DashboardRightWidgetState extends State<DashboardRightWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 450,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            children: [
              ValueListenableBuilder(
                  valueListenable: nativeLanguage,
                  builder: (context, value, widget) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(
                            "images/Language/" + nativeLanguage.value + ".png"),
                        Text(
                          nativeLanguage.value == LanguageOptions.indonesia
                              ? "  Indonesia"
                              : (nativeLanguage.value == LanguageOptions.english
                                  ? "  Inggris"
                                  : "  Korea"),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .merge(TextStyle(fontWeight: FontWeight.w600)),
                        )
                      ],
                    );
                  }),
              Text(
                " - ",
                style: Theme.of(context).textTheme.headline6,
              ),
              ValueListenableBuilder(
                  valueListenable: targetLanguage,
                  builder: (context, value, widget) {
                    return Row(
                      children: [
                        Image.asset(
                            "images/Language/" + targetLanguage.value + ".png"),
                        Text(
                          targetLanguage.value == LanguageOptions.indonesia
                              ? "  Indonesia"
                              : (targetLanguage.value == LanguageOptions.english
                                  ? "  Inggris"
                                  : "  Korea"),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .merge(TextStyle(fontWeight: FontWeight.w600)),
                        )
                      ],
                    );
                  }),
            ],
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.fromLTRB(12, 16, 12, 16),
                  shadowColor: Color.fromARGB(0, 0, 0, 0),
                  primary: Theme.of(context).splashColor),
              onPressed: () {
                Navigator.of(context).push(LanguageDialogRoute(
                    builder: (context) => Center(
                          child: SizedBox(
                            height: 300,
                            width: 500,
                            child: LanguagePopUp(),
                          ),
                        )));
              },
              child: Text(
                "Ganti Bahasa",
                style: Theme.of(context)
                    .textTheme
                    .button!
                    .merge(TextStyle(fontSize: 16)),
              ))
        ],
      ),
    );
  }
}

class LanguageDialogRoute<T> extends PageRoute<T> {
  LanguageDialogRoute({
    required WidgetBuilder builder,
    bool fullscreenDialog = false,
  })  : _builder = builder,
        super(fullscreenDialog: fullscreenDialog);

  final WidgetBuilder _builder;

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => true;

  @override
  Color? get barrierColor => Colors.black54;

  @override
  String? get barrierLabel => "Language Dialog Open";

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return _builder(context);
  }

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 300);
}

class LanguagePopUp extends ConsumerStatefulWidget {
  const LanguagePopUp({Key? key}) : super(key: key);

  @override
  ConsumerState<LanguagePopUp> createState() => _LanguagePopUpState();
}

class _LanguagePopUpState extends ConsumerState<LanguagePopUp> {
  List listLanguage = [LanguageOptions.indonesia, LanguageOptions.english, LanguageOptions.korea];
  List listTargetLanguage = [LanguageOptions.indonesia, LanguageOptions.english];
  String chosenNativeLanguage = nativeLanguage.value;
  String chosenTargetLanguage = targetLanguage.value;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Spacer(flex: 1),
          Text(
            "Ganti Bahasa",
            style: Theme.of(context).textTheme.headline6,
          ),
          Spacer(flex: 2),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Text(
                    "Bahasa Asal",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  DropdownButton(
                      value: chosenNativeLanguage,
                      items: listLanguage.map((e) {
                        return DropdownMenuItem(
                            value: e,
                            child: SizedBox(
                              width: 120,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Image.asset("images/Language/" + e + ".png"),
                                  Text(
                                    e,
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  )
                                ],
                              ),
                            ));
                      }).toList(),
                      onChanged: (lang) {
                        nativeLanguage.value = lang.toString();
                        if (nativeLanguage.value == targetLanguage.value) {
                          if (nativeLanguage.value == LanguageOptions.english) {
                            targetLanguage.value = LanguageOptions.indonesia;
                          } else {
                            targetLanguage.value = LanguageOptions.english;
                          }
                          setState(() {
                            chosenTargetLanguage = targetLanguage.value;
                          });
                        }
                        setState(() {
                          chosenNativeLanguage = nativeLanguage.value;
                        });
                        LangState.updateLanguageOptions(ref, nativeLanguage.value, targetLanguage.value);
                      }),
                ],
              ),
              Text(
                " - ",
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .merge(TextStyle(fontWeight: FontWeight.w400)),
              ),
              Column(
                children: [
                  Text("Bahasa Target",
                      style: Theme.of(context).textTheme.bodyText1),
                  DropdownButton(
                      value: chosenTargetLanguage,
                      items: listTargetLanguage.map((e) {
                          return DropdownMenuItem(
                              value: e,
                              child: SizedBox(
                                width: 120,
                                height: 40,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                        "images/Language/" + e + ".png"),
                                    Text(
                                      e,
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    )
                                  ],
                                ),
                              ));
                      }).toList(),
                      onChanged: (lang) {
                        targetLanguage.value = lang.toString();
                        if (nativeLanguage.value == targetLanguage.value) {
                          if (nativeLanguage.value == LanguageOptions.english) {
                            nativeLanguage.value = LanguageOptions.indonesia;
                          } else {
                            nativeLanguage.value = LanguageOptions.english;
                          }
                          setState(() {
                            chosenNativeLanguage = nativeLanguage.value;
                          });
                        }
                        setState(() {
                          chosenTargetLanguage = targetLanguage.value;
                        });
                        LangState.updateLanguageOptions(ref, nativeLanguage.value, targetLanguage.value);
                      }),
                ],
              ),
            ],
          ),
          Spacer(flex: 3)
        ],
      ),
    );
  }
}
