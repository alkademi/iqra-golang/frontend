import 'package:flutter/material.dart';

class PopUpDialogRoute<T> extends PageRoute<T> {
  PopUpDialogRoute({
    required WidgetBuilder builder,
    bool fullscreenDialog = false,
    int animationDuration = 300,
    required bool barrierDismissible,
  })  : _builder = builder,
        _barrier = barrierDismissible,
        _animationDuration = animationDuration,
        super(fullscreenDialog: fullscreenDialog);

  final WidgetBuilder _builder;
  final bool _barrier;
  final int _animationDuration;

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => _barrier;

  @override
  Color? get barrierColor => Colors.black54;

  @override
  String? get barrierLabel => "PopUp Dialog Open";

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return _builder(context);
  }

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => Duration(milliseconds: _animationDuration);
}
