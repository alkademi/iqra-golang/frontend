import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/services/user.dart';
import 'package:iqra/widgets/popup_dialog_route.dart';

class ForgotPasswordPopUp extends StatefulHookConsumerWidget {
  const ForgotPasswordPopUp({Key? key}) : super(key: key);

  @override
  ConsumerState<ForgotPasswordPopUp> createState() =>
      _ForgotPasswordPopUpState();
}

class _ForgotPasswordPopUpState extends ConsumerState<ForgotPasswordPopUp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool success = false;
  String username = "";
  String email = "";

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(20),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                iconSize: 15,
                splashRadius: 15,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                icon: Icon(Icons.close),
              ),
            ),
            Container(
              // color: Colors.red,
              width: 350,
              height: 250,
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Masukkan pasangan username dan email dari akun kamu untuk me-reset password",
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .merge(TextStyle(fontSize: 16)),
                  ),
                  TextFormField(
                    validator: (value) {
                      if (success) {
                        return null;
                      } else {
                        return "Pasangan username dan email tidak ditemukan";
                      }
                    },
                    onChanged: (value) => {
                      setState(() {
                        username = value;
                      })
                    },
                    decoration: InputDecoration(
                        labelText: "Username",
                        labelStyle: Theme.of(context).textTheme.bodyText2,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0)),
                        isDense: true),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (success) {
                        return null;
                      } else {
                        return "Pasangan username dan email tidak ditemukan";
                      }
                    },
                    onChanged: (value) => {
                      setState(() {
                        email = value;
                      })
                    },
                    decoration: InputDecoration(
                        labelText: "Email",
                        labelStyle: Theme.of(context).textTheme.bodyText2,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0)),
                        isDense: true),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 45, right: 45),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          width: 110,
                          height: 40,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Theme.of(context).splashColor,
                                  shadowColor: Color.fromRGBO(0, 0, 0, 0),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  )),
                              onPressed: () async {
                                final response =
                                    await resetPassword(username, email, ref);
                                if (response.code == 200) {
                                  setState(() {
                                    success = true;
                                  });
                                }

                                if (_formKey.currentState!.validate()) {
                                  Navigator.of(context).pop();
                                  Navigator.of(context).push(PopUpDialogRoute(
                                      barrierDismissible: true,
                                      builder: (context) => Center(
                                            child: SizedBox(
                                              height: 200,
                                              width: 400,
                                              child: NewPasswordSentPopUp(),
                                            ),
                                          )));
                                }
                              },
                              child: Text("Kirim",
                                  style: Theme.of(context)
                                      .textTheme
                                      .button!
                                      .merge(TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 20)))),
                        ),
                        SizedBox(
                          width: 110,
                          height: 40,
                          child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  shadowColor: Color.fromRGBO(0, 0, 0, 0),
                                  side: BorderSide(
                                    width: 1.5,
                                    color: Theme.of(context).splashColor,
                                  )),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                "Batal",
                                style: Theme.of(context)
                                    .textTheme
                                    .button!
                                    .merge(TextStyle(
                                        color: Theme.of(context).splashColor,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20)),
                              )),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NewPasswordSentPopUp extends StatelessWidget {
  const NewPasswordSentPopUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "Password baru sudah dikirimkan ke email anda. Silahkan login kembali dengan password tersebut.",
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .merge(TextStyle(fontSize: 16)),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("Selesai",
                  style: Theme.of(context).textTheme.button!.merge(
                      TextStyle(fontWeight: FontWeight.w500, fontSize: 20))),
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).splashColor),
            )
          ],
        ),
      ),
    );
  }
}
