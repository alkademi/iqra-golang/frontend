import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/provider/auth_state.dart';
import 'package:restart_app/restart_app.dart';

class MyDrawer extends HookConsumerWidget {
  const MyDrawer(this._scaffoldKey, {Key? key}) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topRight: Radius.circular(16),
        bottomRight: Radius.circular(16),
      ),
      child: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        backgroundColor: Theme.of(context).primaryColor,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: const EdgeInsets.all(24),
          children: [
            Row(
              children: [
                IconButton(
                  padding: EdgeInsets.zero,
                  onPressed: () {
                    AuthState.logout(ref);
                    Restart.restartApp();
                  },
                  icon: const Icon(
                    Icons.door_back_door_outlined,
                    color: Colors.white,
                    size: 40,
                  ),
                ),
                const Spacer(
                  flex: 1,
                ),
                IconButton(
                  padding: EdgeInsets.zero,
                  onPressed: () {
                    _scaffoldKey.currentState!.openEndDrawer();
                  },
                  icon: const Icon(
                    Icons.cancel,
                    color: Colors.white,
                    size: 40,
                  ),
                )
              ],
            ),
            const SizedBox(height: 64),
            NavItem(
              text: "Beranda",
              handleOnTap: (context) {
                Navigator.pushNamed(context, '/');
              },
              leading: const Icon(
                Icons.home,
                color: Colors.white,
                size: 28,
              ),
            ),
            NavItem(
              text: "Leaderboard",
              handleOnTap: (context) {
                Navigator.pushNamed(context, '/leaderboard');
              },
              leading: const Icon(
                Icons.bar_chart,
                color: Colors.white,
                size: 32,
              ),
            ),
            NavItem(
              text: "Profil",
              handleOnTap: (context) {
                Navigator.pushNamed(context, '/profile');
              },
              leading: const Icon(
                Icons.person,
                color: Colors.white,
                size: 28,
              ),
            ),
            NavItem(
              text: "Pencapaian",
              handleOnTap: (context) {
                Navigator.pushNamed(context, '/pencapaian');
              },
              leading: const Icon(
                Icons.star,
                color: Colors.white,
                size: 28,
              ),
            ),
            NavItem(
              text: "Pengaturan",
              handleOnTap: (context) {
                Navigator.pushNamed(context, '/pengaturan');
              },
              leading: const Icon(
                Icons.settings,
                color: Colors.white,
                size: 28,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NavItem extends StatelessWidget {
  const NavItem({
    Key? key,
    required this.handleOnTap,
    required this.text,
    required this.leading,
  }) : super(key: key);

  final Function handleOnTap;
  final String text;
  final Widget leading;

  @override
  Widget build(BuildContext context) {
    final navTextStyle = (Theme.of(context).textTheme.headline6!)
        .merge(const TextStyle(color: Colors.white));

    return ListTile(
      leading: leading,
      title: Text(
        text,
        style: navTextStyle,
      ),
      onTap: () => handleOnTap(context),
      contentPadding: const EdgeInsets.symmetric(
        vertical: 12,
      ),
    );
  }
}
