import 'package:flutter/material.dart';
import 'package:flip_card/flip_card.dart';
import 'package:iqra/model/dictionary.dart';

class SpecialFlashcard extends Flashcard {
  SpecialFlashcard({Key? key})
      : super(
          key: key,
          word: FlashcardWord(
            id: 1,
            nativeWord: "nativeWord",
            targetWord: "targetWord",
            targetDefinition: "targetDefinition",
            nativeLang: "nativeLang",
            targetLang: "targetLang",
          ),
        );

  @override
  Widget build(BuildContext context) {
    final Color bgFrontColor = Theme.of(context).primaryColor;
    const Color borderFrontColor = Colors.white;

    return FlipCard(
      fill: Fill.fillBack,
      front: Container(
        width: 335,
        height: 490,
        padding: EdgeInsets.all(32),
        decoration: BoxDecoration(
          color: bgFrontColor,
          borderRadius: BorderRadius.all(Radius.circular(32)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 16,
              spreadRadius: 0,
              offset: Offset(0, 0),
            ),
          ],
        ),
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: bgFrontColor,
            borderRadius: BorderRadius.all(Radius.circular(32)),
            border: Border.all(
              color: borderFrontColor,
              width: 3,
            ),
          ),
          child: Center(
            child: Image.asset(
              "icons/iqra-logo.png",
              width: 128,
            ),
          ),
        ),
      ),
      back: Card(
        isBack: true,
        title:
            "Swipe to Right if you mastered the word! Swipe to Left if you still need to practice the word!",
        bgColor: Colors.white,
        borderColor: Theme.of(context).primaryColor,
      ),
    );
  }
}

class Flashcard extends StatelessWidget {
  Flashcard({Key? key, required this.word}) : super(key: key);

  final FlashcardWord word;

  @override
  Widget build(BuildContext context) {
    return FlipCard(
      fill: Fill.fillBack,
      front: Card(
        title: word.nativeWord,
        bgColor: Theme.of(context).primaryColor,
        borderColor: Colors.white,
      ),
      back: Card(
        isBack: true,
        title: word.targetDefinition,
        bgColor: Colors.white,
        borderColor: Theme.of(context).primaryColor,
      ),
    );
  }
}

class Card extends StatelessWidget {
  Card(
      {Key? key,
      required this.title,
      required this.bgColor,
      required this.borderColor,
      this.isBack = false})
      : super(key: key);

  final String title;
  final Color bgColor;
  final Color borderColor;
  bool isBack;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 335,
      height: 490,
      padding: EdgeInsets.all(32),
      decoration: BoxDecoration(
        color: bgColor,
        borderRadius: BorderRadius.all(Radius.circular(32)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 16,
            spreadRadius: 0,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.all(Radius.circular(32)),
          border: Border.all(
            color: borderColor,
            width: 3,
          ),
        ),
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: isBack
                ? Theme.of(context).textTheme.headline6!.merge(
                      TextStyle(
                        color: borderColor,
                      ),
                    )
                : Theme.of(context).textTheme.headline4!.merge(
                      TextStyle(
                        color: borderColor,
                      ),
                    ),
          ),
        ),
      ),
    );
  }
}
