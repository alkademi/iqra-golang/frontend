import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:iqra/provider/user_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

class JwtNotifier extends StateNotifier<String> {
  JwtNotifier(this.ref, this.string) : super(string);

  final Ref ref;
  final String string;

  void update(String newJwt) => state = newJwt;
}

class AuthState {
  static late SharedPreferences storage;
  static late StateNotifierProvider<JwtNotifier, String> jwtProvider;

  static Future<void> init() async {
    storage = await SharedPreferences.getInstance();

    if (storage.containsKey('jwt')) {
      String? savedJwt = storage.getString('jwt');

      // if (JwtDecoder.isExpired(savedJwt)) {
      //   storage.remove('jwt');
      // } else {
      jwtProvider = StateNotifierProvider<JwtNotifier, String>(
          (ref) => JwtNotifier(ref, savedJwt!));
      // ?
    } else {
      jwtProvider = StateNotifierProvider<JwtNotifier, String>(
          (ref) => JwtNotifier(ref, ""));
    }
  }

  static Future<void> login(
    String username,
    String password,
    String token,
    WidgetRef ref,
  ) async {
    ref.watch(jwtProvider.notifier).update(token);

    await storage.setString('jwt', token);
  }

  static void logout(WidgetRef ref) {
    ref.watch(jwtProvider.notifier).update("");
    ref.watch(UserState.userProvider.notifier).update(null);
    storage.clear();
  }
}
