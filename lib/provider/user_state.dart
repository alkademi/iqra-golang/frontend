import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:iqra/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserNotifier extends StateNotifier<User?> {
  UserNotifier(this.ref, this.user) : super(user);

  final Ref ref;
  final User? user;

  void update(User? newUser) {
    state = newUser;
    if (newUser != null) {
      UserState.storage.setString("user", jsonEncode(newUser));
    }
  }
}

class UserState {
  static late SharedPreferences storage;
  static late StateNotifierProvider<UserNotifier, User?> userProvider;

  static Future<void> init() async {
    storage = await SharedPreferences.getInstance();
    if (storage.containsKey('user')) {
      User savedUser = User.fromJson(jsonDecode(storage.getString("user")!));

      userProvider = StateNotifierProvider<UserNotifier, User?>(
          (ref) => UserNotifier(ref, savedUser));
    } else {
      userProvider = StateNotifierProvider<UserNotifier, User?>(
          (ref) => UserNotifier(ref, null));
    }
  }

  static Future<void> createAccount(
    User user,
    WidgetRef ref,
  ) async {
    ref.watch(userProvider.notifier).update(user);
    await storage.setString('user', jsonEncode(user));
  }

  static void removeUser(WidgetRef ref) {
    ref.watch(userProvider.notifier).update(null);
    storage.remove('user');
  }
}
