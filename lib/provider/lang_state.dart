import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tuple/tuple.dart';

class LangNotifier extends StateNotifier<Tuple2<String, String>> {
  // Item1 of the Tuple as Native Language
  // Item2 of the Tuple as Target Language
  LangNotifier(this.ref, this.pairLang) : super(pairLang);

  final Ref ref;
  final Tuple2<String, String> pairLang;

  void update(Tuple2<String, String> pairLang) => state = pairLang;
}

class LanguageOptions {
  static const String english = "eng";
  static const String indonesia = "indo";
  static const String korea = "kor";
}

class LangState {
  static StateNotifierProvider<LangNotifier, Tuple2<String, String>>
      langProvider = StateNotifierProvider(
    (ref) => LangNotifier(
      ref,
      Tuple2(
        LanguageOptions.english,
        LanguageOptions.indonesia,
      ),
    ),
  );

  static void updateLanguageOptions(
      WidgetRef ref, String natLang, String targetLang) {
    ref
        .watch(langProvider.notifier)
        .update(Tuple2<String, String>(natLang, targetLang));
  }
}
