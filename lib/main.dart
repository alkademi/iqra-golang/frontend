import 'package:flutter/material.dart';
import 'package:iqra/model/user.dart';
import 'package:iqra/provider/user_state.dart';
import 'package:iqra/screens/achievement.dart';
import 'package:iqra/screens/aksaraKorea.dart';
import 'package:iqra/screens/dashboard_screen.dart';
import 'package:iqra/screens/flashcard_screen.dart';
import 'package:iqra/screens/landing_slider_screen.dart';
import 'package:iqra/screens/leaderboard.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:iqra/screens/loginTablet.dart';
import 'package:iqra/screens/signupTablet.dart';
import 'package:iqra/screens/user_setting.dart';
import 'package:iqra/services/user.dart';
import 'package:iqra/widgets/dashboard_right_widget.dart';
import 'package:iqra/widgets/dictionary_right_widget.dart';
import 'package:iqra/widgets/leaderboard_right_widget.dart';
import 'package:iqra/screens/kamus.dart';
import 'package:iqra/widgets/signup_verification_widget.dart';

import 'screens/leaderboard.dart';
import 'screens/profile_screen.dart';
import 'screens/landing_slider_screen.dart';

import 'widgets/my_app_bar.dart';
import 'widgets/my_drawer.dart';

import 'provider/auth_state.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AuthState.init();
  await UserState.init();
  runApp(
    ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends HookConsumerWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final String _jwt = ref.watch(AuthState.jwtProvider);
    User? _user = ref.watch(UserState.userProvider);

    return MaterialApp(
      title: 'Flutter Demo',
      onGenerateRoute: (settings) {
        if (_jwt != "") {
          getUser(_user!.username).then((value) {
            UserState.createAccount(value!, ref);
          });
          return PageRouteBuilder(
            pageBuilder: (_, __, ___) => BaseScreen(
              settings.name == '/'
                  ? DashboardScreen()
                  : settings.name == '/profile'
                      ? ProfileScreen()
                      : settings.name == '/leaderboard'
                          ? Leaderboard()
                          : settings.name == '/pencapaian'
                              ? Achievement()
                              : settings.name == '/aksara-korea'
                                  ? AksaraKorea()
                                  : settings.name == '/kamus'
                                      ? Ka()
                                      : settings.name == '/flashcard'
                                          ? FlashcardScreen()
                                          : settings.name == '/pengaturan'
                                              ? UserSetting()
                                              : Leaderboard(),
              settings.name == '/'
                  ? "iqra"
                  : settings.name == '/profile'
                      ? "Profile Saya"
                      : settings.name == '/leaderboard'
                          ? "Leaderboard"
                          : settings.name == '/pencapaian'
                              ? "Pencapaian Saya"
                              : settings.name == '/aksara-korea'
                                  ? "Aksara Korea"
                                  : settings.name == '/flashcard'
                                      ? "Quiz"
                                      : settings.name == '/kamus'
                                          ? "Kamus"
                                          : "Pengaturan",
              settings.name == "/"
                  ? DashboardRightWidget()
                  : settings.name == '/leaderboard'
                      ? LeaderboardRightWidget()
                      : settings.name == '/aksara-korea'
                          ? DictionaryRightWidget()
                          : settings.name == '/pencapaian'
                              ? DictionaryRightWidget()
                              : settings.name == '/kamus' ||
                                      settings.name == "/flashcard"
                                  ? DictionaryRightWidget()
                                  : null,
              settings.name == "/flashcard" ? Color(0xFFEBE5FF) : Colors.white,
            ),
          );
        }
        return PageRouteBuilder(
          pageBuilder: (_, __, ___) => NotLoggedInBaseScreen(
            settings.name == '/'
                ? (_user != null ? SignUpTablet() : LandingSliderScreen())
                : settings.name == '/login'
                    ? LoginTablet()
                    : settings.name == '/register'
                        ? SignUpTablet()
                        : SizedBox.shrink(),
          ),
        );
      },
      theme: ThemeData(
        // To use theme,
        // e.g. => Theme.of(context).splashColor

        // Global Colors
        primarySwatch: Colors.deepPurple,
        primaryColor: const Color(0xFF5840BA),
        splashColor: const Color(0xFFFB9E8A),
        hintColor: const Color(0xFFB0EFEB),
        // Global Text Style
        textTheme: const TextTheme(
          bodyText1: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
          ),
          headline1: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 72,
            color: Colors.black,
          ),
          headline2: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 50,
            color: Colors.black,
          ),
          headline3: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 45,
            color: Colors.black,
          ),
          headline4: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 36,
            color: Colors.black,
          ),
          headline5: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 30,
            color: Colors.black,
          ),
          headline6: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 26,
          ),
          button: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 24,
            color: Colors.white,
          ),
          overline: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            decoration: TextDecoration.underline,
            color: Color(0xFF5840BA),
          ),
          bodyText2: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            color: Color.fromRGBO(172, 172, 172, 1),
          ),
        ),
        fontFamily: 'Poppins',
      ),
    );
  }
}

class NotLoggedInBaseScreen extends StatelessWidget {
  const NotLoggedInBaseScreen(
    this.pageContent, {
    Key? key,
  }) : super(key: key);

  final Widget pageContent;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageContent,
    );
  }
}

class BaseScreen extends StatelessWidget {
  BaseScreen(this.pageContent, this.title, this.rightWidgetAppBar,
      this.backgroundColor,
      {Key? key})
      : super(key: key);

  final Widget pageContent;
  final String title;
  final Widget? rightWidgetAppBar;
  final Color? backgroundColor;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: backgroundColor,
      drawer: MyDrawer(_scaffoldKey),
      appBar: MyAppBar(_scaffoldKey, title, rightWidgetAppBar, backgroundColor),
      body: pageContent,
    );
  }
}
