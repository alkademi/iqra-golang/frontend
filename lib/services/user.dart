import 'dart:convert';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:iqra/model/general_response.dart';
import 'package:iqra/provider/auth_state.dart';
import 'package:iqra/constant/index.dart';
import 'package:iqra/provider/user_state.dart';

import '../model/user.dart';

Future<GeneralResponse> login(
  String username,
  String password,
  WidgetRef ref,
) async {
  final apiURL = Uri.parse('$API_URL/login');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode(
      {
        "username": username,
        "password": password,
      },
    ),
  );

  final responseBody = jsonDecode(result.body);

  if (responseBody['code'] == 200) {
    AuthState.login(username, password, responseBody['data']['token'], ref);

    User? user = await getUser(username);

    await UserState.createAccount(user!, ref);
  }

  return GeneralResponse(
    code: responseBody['code'],
    message: responseBody['message'],
    data: responseBody['data'],
  );
}

Future<GeneralResponse> signUp(
  String username,
  String password,
  String fullName,
  String email,
  String birthDate,
  WidgetRef ref,
) async {
  final apiURL = Uri.parse('$API_URL/account/create');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode(
      {
        "username": username,
        "password": password,
        "full_name": fullName,
        "email": email,
        "birth_date": birthDate
      },
    ),
  );

  final responseBody = jsonDecode(result.body);

  if (responseBody['code'] == 200) {
    // print(responseBody['data']['regis_token']);
    User user = User(
        username: responseBody['data']['username'],
        email: responseBody['data']['email'],
        birthDate: DateTime.parse(responseBody['data']['birth_date']),
        experience: responseBody['data']['exp'],
        wordLearned: responseBody['data']['word_learned'],
        fullName: responseBody['data']['full_name'],
        rank: responseBody['data']['rank']);

    UserState.createAccount(user, ref);
  }

  return GeneralResponse(
    code: responseBody['code'],
    message: responseBody['message'],
    data: responseBody['data'],
  );
}

Future<GeneralResponse> validateAccount(
  String username,
  String? regisToken,
  WidgetRef ref,
) async {
  final apiURL = Uri.parse('$API_URL/account/validate');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode(
      {"username": username, "regis_token": regisToken},
    ),
  );

  final responseBody = jsonDecode(result.body);

  if (responseBody['code'] == 200) {
    // login(username, password, ref)

    // TODO: Benerin
    // Ngambil jwt harus pake password, padahal passwordnya gk disimpan di storage,
    // kalo dari response['data'] passwordnya udah di enkripsi
    AuthState.login(username, "", "token", ref);
  }

  return GeneralResponse(
    code: responseBody['code'],
    message: responseBody['message'],
    data: responseBody['data'],
  );
}

Future<GeneralResponse> deleteAccount(
  String username,
  WidgetRef ref,
) async {
  final apiURL = Uri.parse('$API_URL/account/delete');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode(
      {"username": username},
    ),
  );

  final responseBody = jsonDecode(result.body);

  if (responseBody['code'] == 200) {
    UserState.removeUser(ref);
  }

  return GeneralResponse(
    code: responseBody['code'],
    message: responseBody['message'],
    data: responseBody['data'],
  );
}

Future<User?> getUser(String username) async {
  final apiURL = Uri.parse('$API_URL/account/get_one');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode(
      {
        "username": username,
      },
    ),
  );

  final responseBody = jsonDecode(result.body);

  print(responseBody['data']);

  if (responseBody['code'] == 200) {
    return User.fromJson(responseBody['data']);
  }

  return null;
}

Future<List<User>> getLeaderboard(
  WidgetRef ref,
) async {
  final apiURL = Uri.parse('$API_URL/leaderboard');

  final result = await http.get(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
  );

  final responseBody = jsonDecode(result.body);

  if (responseBody['code'] == 200) {
    return (responseBody['data'] as List)
        .map((json) => User.fromJson(json))
        .toList();
  }

  return [];
}

Future<GeneralResponse> resetPassword(
  String username,
  String email,
  WidgetRef ref,
) async {
  final apiURL = Uri.parse('$API_URL/account/reset_password');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode(
      {"username": username, "email": email},
    ),
  );

  final responseBody = jsonDecode(result.body);

  return GeneralResponse(
    code: responseBody['code'],
    message: responseBody['message'],
    data: responseBody['data'],
  );
}

Future<GeneralResponse> changePassword(
  String username,
  String oldPassword,
  String newPassword,
  WidgetRef ref,
) async {
  final apiURL = Uri.parse('$API_URL/account/change_password');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode(
      {
        "username": username,
        "old_password": oldPassword,
        "new_password": newPassword,
      },
    ),
  );

  final responseBody = jsonDecode(result.body);
  print(responseBody);
  return GeneralResponse(
    code: responseBody['code'],
    message: responseBody['message'],
    data: responseBody['data'],
  );
}
