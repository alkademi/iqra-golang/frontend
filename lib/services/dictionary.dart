// ALL API CALLING FOR DICTIONARY
import 'dart:convert';
import 'dart:developer';
import 'package:iqra/constant/index.dart';

import 'package:iqra/model/dictionary.dart';
import 'package:http/http.dart' as http;
import 'package:iqra/model/general_response.dart';
import 'package:iqra/model/user.dart';
import 'package:iqra/widgets/dashboard_right_widget.dart';

class KamusResult {
  int code;
  String message;
  Dictionary? data;

  KamusResult({required this.code, required this.message, this.data});

  factory KamusResult.createKamusResult(Map<String, dynamic> object) {
    if (object['data'] == null) {
      log(object['data'].toString());
      return KamusResult(
        code: object['code'],
        message: object['message'],
        data: null,
      );
    } else {
      // print(object['data'][0].toString());
      // print("ajspaaaaa");
      // final resp = jsonDecode(object['data'][0]);
      // print("ajspa");
      return KamusResult(
        code: object['code'],
        message: object['message'],
        data: Dictionary.fromJson(object['data']),
      );
    }
  }

  static Future<KamusResult> getKamus(query, page) async {
    var nat = nativeLanguage.value;
    var targ = targetLanguage.value;
    if (nat == "IND") {
      nat = "indo";
    } else {
      nat = nat.toLowerCase();
    }

    if (targ == "IND") {
      targ = "indo";
    } else {
      targ = targ.toLowerCase();
    }
    // var apiURLnormal = Uri.parse("http://128.199.247.11:8080/dict/get-words-id");
    // print(query);
    if (query == "") {
      var apiURLnormal = Uri.parse("$API_URL/dict/get-words-id");
      var res = await http.post(apiURLnormal,
          headers: {
            'Content-Type': 'application/json',
          },
          body: jsonEncode({
            "dict_id": page,
            "native_lang": nat,
            "target_lang": targ,
          }));
      return KamusResult.createKamusResult(jsonDecode(res.body));
    } else {
      var apiURLquery = Uri.parse("$API_URL/dict/get-words");

      var res = await http.post(apiURLquery,
          headers: {
            'Content-Type': 'application/json',
          },
          body: jsonEncode({
            "native_word": query,
            "native_lang": nat,
            "target_lang": targ,
          }));
      return KamusResult.createKamusResult(jsonDecode(res.body));
    }
  }
}

Future<List<FlashcardWord>> getRandomFlashcard(
    User user, String natLang, String targetLang) async {
  final apiURL = Uri.parse('$API_URL/dict/get-flashcard-random');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
    },
    body: jsonEncode(
      {
        "username": user.username,
        "native_lang": natLang,
        "target_lang": targetLang,
      },
    ),
  );

  final responseBody = jsonDecode(result.body);

  if (responseBody['code'] == 200) {
    return (responseBody['data'] as List)
        .map((json) => FlashcardWord.fromJson(json))
        .toList();
  }

  return [];
}

Future<GeneralResponse> addWordLearned(String username, int dictId,
    String natLang, String targetLang, String token) async {
  final apiURL = Uri.parse('$API_URL/account/add_exp');

  final result = await http.post(
    apiURL,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    },
    body: jsonEncode(
      {
        "username": username,
        "dict_id": dictId,
        "native_lang": natLang,
        "target_lang": targetLang,
      },
    ),
  );

  final responseBody = jsonDecode(result.body);

  return GeneralResponse(
      code: responseBody['code'], message: responseBody['message']);
}
